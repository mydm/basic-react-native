import React, { Component } from 'react';
import {Image, SafeAreaView, Text, TextInput, View, StyleSheet, Button} from 'react-native';
import iphone from './iphone12.jpg';

const App = () => {
  return <SampleStylingComponent />;
};

const SampleStylingComponent = () => {
  return (
    <SafeAreaView>
      <View 
      style= {{ 
        padding: 12,
        backgroundColor: '#F2F2F2',
        width: 212,
        borderRadius: 8,
        margin: 12
       }}>

        <Image 
        source= {iphone}
        style= {{ width: 188, height: 107, borderRadius: 8 }}
        />

        <Text 
        style= {{ fontSize: 14, fontWeight: 'bold', marginTop: 12 }}
        >Iphone 12 Pro
        </Text>

        <Text 
        style= {{ fontSize: 12, color: 'orange',fontWeight: 'bold', marginTop: 12 }}
        >Rp25.000.000,-
        </Text>

        <Text 
        style= {{ 
          fontSize: 12,
          fontWeight: '300', 
          marginTop: 12 }}
        >Karawang
        </Text>
        
        <View style={{ backgroundColor: '#6FCF97', borderRadius: 25, marginTop: 25 }}>
          <Text style={{ 
            fontSize: 16, 
            fontWeight: 'bold', 
            color: 'white', 
            textAlign: 'center',
            paddingVertical: 6
          }}
          >Beli
          </Text>
        </View>        
      </View>
    </SafeAreaView>
  );
};

const StylingRNComponent = () => {
  return (
    <SafeAreaView>
      <View>
        <Text style={styles.text}>Styling Component</Text>
        <View style={{ 
          width: 100,
          height: 100,
          backgroundColor: '#d68060',
          borderWidth: 2,
          borderColor: 'black',
          marginTop: 20,
          marginLeft: 20
          }}/>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#d68060',
    marginLeft: 20,
    marginTop: 20,
  },
}); 

const SampleComponent = () => {
  return (
  <SafeAreaView>
    <View>
      <View style={{ width: 80, height: 80, backgroundColor: '#d68060' }}/>
      <Photo />
      <DM />
      <Text>email:</Text>
      <Text>password</Text>
      <Text>confirm password</Text>
      <TextInput style={{ borderWidth: 1 }}/>
      <BoxGreen />
    </View>
    </SafeAreaView>
  );
};

const DM = () => {
  return <Text>Agustian DM</Text>;
};

const Photo = () => {
  return <Image 
    source={{ url: 'https://placeimg.com/100/100/tech' }}
    style={{ width: 100, height: 100 }}
  />
}

class BoxGreen extends Component {
  render() {
    return <Text>Ini text dari class</Text>;
  }
}
export default App;